import argparse
import os
import string

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")
parser.add_argument("-i", "--input", required=True, type=str, help="path to the directory where tf resource files are stored")
parser.add_argument("-o", "--output", required=True, type=str, help="path to the directory where resultant schema files need to"
                                                " be stored")

args = parser.parse_args()
RESOURCE_PREFIX = "resource_"

REPLACEMENT_MAP = {
    'Type:schema.TypeString': '\"type\":\"string\"',
    'Type:schema.TypeMap': '\"type\":\"dict\"',
    'Type:schema.TypeString,': '\"type\":\"string\"',
    'Type:schema.TypeMap,': '\"type\":\"dict\"',
    '{': '{',
    '}': '},'
}

"""
Process contents of a given resource tf file and extract schema information 
"""


def process_file(file_path=None, dst_dir=None):

    with open(file_path) as file:
        content = file.read()
        content = content.translate({ord(c): None for c in string.whitespace})
        json_schema_str = []
        if not content.__contains__("Schema:map[string]*schema.Schema"):
            print("%s does not contain schema data" % file_path)
        else:
            print("Dumping content below: ")
            json_start_index = content.find("Schema:map[string]*schema.Schema") + 32
            json_count = 0
            for c in content[json_start_index: len(content)]:
                if c == '{':
                    json_count = json_count + 1
                if c == '}':
                    json_count = json_count - 1
                # otherwise keep appending
                json_schema_str.append(c)
                if json_count == 0:
                    print("Found json string: %s" % "".join(json_schema_str))
                    break
            if len(json_schema_str) > 0:
                with open(os.path.join(dst_dir, os.path.basename(file_path)), mode='w') as output:
                    output.write(''.join(json_schema_str))
                    print("write complete for file: %s" % file_path)


if __name__ == "__main__":
    file_count = 0
    print("Starting to read files in path: %s" % args.input)

    for root, subdirs, files in os.walk(args.input):
        for file in files:
            if file.startswith(RESOURCE_PREFIX):
                process_file(os.path.join(root, file), args.output)
                file_count = file_count + 1
    print("Done processing %d files and dumped to local store" % file_count)
